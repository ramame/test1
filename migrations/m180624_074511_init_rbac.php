<?php

use yii\db\Migration;

/**
 * Class m180624_074511_init_rbac
 */
class m180624_074511_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $employee = $auth->createRole('employee');
        $auth->add($employee);
  
        $manager = $auth->createRole('manager');
        $auth->add($manager);

        $auth->addChild($manager, $employee);
 
        $seeTask = $auth->createPermission('seeTask');
        $auth->add($seeTask);

        $viewUsers = $auth->createPermission('viewUsers');
        $auth->add($viewUsers);  

        $updateUsers = $auth->createPermission('updateUsers');
        $auth->add($updateUsers);

        $manageTask = $auth->createPermission('manageTask');
        $auth->add($manageTask);

        $updateOwnUser = $auth->createPermission('updateOwnUser');
    
        $rule = new \app\rbac\EmployeeRule;
        $auth->add($rule);
            
        $updateOwnUser->ruleName = $rule->name;                 
        $auth->add($updateOwnUser);  

            $auth->addChild($manager, $updateUsers);
            $auth->addChild($manager, $manageTask);
            $auth->addChild($employee, $seeTask);
            $auth->addChild($employee, $updateOwnUser);
            $auth->addChild($employee, $viewUsers);
        
            $auth->addChild($updateOwnUser, $updateUsers);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_074511_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_074511_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
